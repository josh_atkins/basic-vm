#! /bin/bash


# Set time
echo "Europe/London" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
locale-gen en_US.UTF-8
dpkg-reconfigure locales


apt-get update
# apt-get install -y "curl" "wget" "build-essential" "software-properties-common" "git" "libxml2-dev" "libxslt1-dev" "libjpeg-dev" "zlib1g-dev" "python-dev" "libffi-dev" "libssl-dev" "libmysqlclient-dev" "python-pip"
#pip install "pyopenssl" "ndg-httpsclient" "pyasn1" "virtualenv" "virtualenvwrapper"



apt-get install -y "wget" "git"

wget -qO- https://get.docker.com/ | sh

